package com.allstate.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentEntitiyTest {
    private Payment payment;
    private Date localDate = new Date();

    @BeforeEach
    void setUp() {
        payment = new Payment(1, "Cash", localDate, 500, 1234);
    }

    @Test
    void getIdTest() {
        assertEquals(1, payment.getId());
    }


    @Test
    void getAmountTest() {
        assertEquals(500.00, payment.getAmount());
    }

    @Test
    void getTypeTest() {
        assertEquals("Cash", payment.getType());
    }

    @Test
    void getCustIdTest() {
        assertEquals(1234, payment.getCustId());
    }

    @Test
    void getDateTest() {
        assertEquals(localDate, payment.getPaymentDate());
    }
}
