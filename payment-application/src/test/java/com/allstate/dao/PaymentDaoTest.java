package com.allstate.dao;


import com.allstate.entities.Payment;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PaymentDaoTest {
    @Autowired
    private PaymentDao dao;

    @Autowired
    MongoTemplate template;

    @Test
    public void rowCountTest() {

        assertEquals(template.count(new Query(), Payment.class), dao.rowCount());
    }

    @Test
    public void saveTest() {
        Payment payment = new Payment(1, "Cash", new Date(), 500, 1234);
        assertEquals(payment.getId(), dao.save(payment));
    }

    @Test
    public void findByIdTest() {
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/mm/yyyy hh:mm:ss");
        Date date=new Date();
        String dateStr=simpleDateFormat.format(date);
         Payment payment = new Payment(2, "Cash",date, 500, 1234);
        dao.save(payment);
        assertEquals(payment.getId(), dao.findById(2).getId());
        assertEquals(dateStr,simpleDateFormat.format(payment.getPaymentDate()));

    }

    @Test
    public void findByTypeTest() {
        Date date = new Date();
        int count = dao.findByType("CC").size();
        Payment payment = new Payment(3, "CC", date, 500, 1234);
        dao.save(payment);
        Payment payment2 = new Payment(4, "CC", date, 500, 1234);
        dao.save(payment2);
        assertEquals(count + 2, dao.findByType("CC").size());
    }

    @AfterAll
    public void Cleanup() {
        Payment payment = dao.findById(1);
        template.remove(payment);
        payment = dao.findById(2);
        template.remove(payment);
        payment = dao.findById(3);
        template.remove(payment);
        payment = dao.findById(4);
        template.remove(payment);

    }

}
