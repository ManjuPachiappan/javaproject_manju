package com.allstate.service;


import com.allstate.dao.PaymentDao;
import com.allstate.entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
public class PaymentBsTest {

    @MockBean
    private PaymentDao dao;

    @Autowired
    PaymentService paymentBS;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    public void rowCountTest(){
        doReturn(10).when(dao).rowCount();
        assertEquals(10,paymentBS.rowCount());
    }



    @Test
    public void savePaymentTest() {

        Payment payment = new Payment(1, "Cash", new Date(), 500, 1234);
        doReturn(payment.getId()).when(dao).save(payment);
        assertEquals(payment.getId(),paymentBS.save(payment));
    }

    @Test
    public void findByIdTest() {
        Payment payment = new Payment(1, "Cash", new Date(), 500, 1234);
        doReturn(payment).when(dao).findById(1);
        assertEquals(payment, paymentBS.findById(1));
    }
    @Test
    public void findByIdZeroTest() {
        assertNull(paymentBS.findById(0));
    }

    @Test
    public void findByTypeTest() {
        List<Payment> paymentList= new ArrayList<>();
        paymentList.add(new Payment(1, "CC", new Date(), 500, 1234));
        doReturn(paymentList).when(dao).findByType("CC");
        assertEquals( paymentList, paymentBS.findByType("CC"));
    }
    @Test
    public void findByTypeNullTest() {

        assertNull(paymentBS.findByType(null));
    }

}
