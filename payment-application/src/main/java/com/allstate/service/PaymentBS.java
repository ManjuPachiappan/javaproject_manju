package com.allstate.service;

import com.allstate.dao.PaymentDao;
import com.allstate.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentBs implements PaymentService{
    @Autowired
    PaymentDao dao;
    @Override
    public int rowCount() {
        return dao.rowCount();
    }

    @Override
    public Payment findById(int id) {
        if(id>0) {
            return dao.findById(id);
        }else{
           return null;
        }
    }

    @Override
    public List<Payment> findByType(String type) {
        if(null!=type) {
            return dao.findByType(type);
        }else{
            return null;
        }
    }

    @Override
    public int save(Payment payment) {
        return dao.save(payment);
    }
}
