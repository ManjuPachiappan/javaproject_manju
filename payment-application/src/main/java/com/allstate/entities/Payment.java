package com.allstate.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Payment {

    @Id
    private int id;
    private String type;
    private Date paymentDate;
    private double amount;
    private int custId;

    public Payment() {
    }

    public Payment(int id, String type, Date paymentDate, double amount, int custId) {
        this.id = id;
        this.type = type;
        this.paymentDate = paymentDate;
        this.amount = amount;
        this.custId = custId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }

    public String toString() {

        return "Id: " + this.id + " Type: " + this.type + " Payment Date: " + this.paymentDate + "Payment Amount: " + this.amount + "Customer Id: " + this.custId;
    }


}
