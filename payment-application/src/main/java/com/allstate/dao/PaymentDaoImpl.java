package com.allstate.dao;

import com.allstate.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentDaoImpl implements PaymentDao {
    @Autowired
    MongoTemplate template;

    @Override
    public int rowCount() {
        Query query = new Query();
        int result= (int) template.count(query, Payment.class);
        return result;

    }

    @Override
    public Payment findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Payment payment = template.findOne(query, Payment.class);
        return payment;
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        List<Payment> paymentList = template.find(query, Payment.class);
        return paymentList;
    }

    @Override
    public int save(Payment payment) {
        payment= template.save(payment);
      return payment.getId();
    }
}
