package com.allstate.dao;

import com.allstate.entities.Payment;

import java.util.List;

public interface PaymentDao {
    int rowCount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);
}
