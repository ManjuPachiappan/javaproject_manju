package com.allstate.rest;

import com.allstate.entities.Payment;
import com.allstate.service.PaymentBs;
import com.allstate.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/payment-api")
@CrossOrigin(origins = "http://localhost:3000")
public class PaymentControllerImpl implements PaymentController{
    @Autowired
    PaymentService paymentService;
    Logger logger= LoggerFactory.getLogger(PaymentControllerImpl.class);

    @Override
    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getStatus() {
        return "Payment Rest API is Up and Running";
    }
    @Override
    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public int rowCount() {
        logger.info("Row count method called");
        return paymentService.rowCount();
    }

    @Override
    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    public ResponseEntity<Payment> findById(@PathVariable("id")int id) {
       Payment payment= paymentService.findById(id);
        if (payment ==null) {
            logger.info("Payment data is null for "+id);
            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
        }
        else {
            logger.info("Valid data found for id: "+id +" Payment details are "+payment);
            return new ResponseEntity<Payment>(payment, HttpStatus.OK);
        }
    }

    @Override
    @RequestMapping(value = "/findbytype/{type}", method = RequestMethod.GET)
    public ResponseEntity<List<Payment>> findByType(@PathVariable("type")String type) {
       List<Payment> paymentList= paymentService.findByType(type);
        if (null== paymentList || paymentList.size()==0) {
            logger.info("Payment data is null for type"+type);
            return new ResponseEntity<List<Payment>>(HttpStatus.NOT_FOUND);
        }
        else {
            logger.info("Valid data found for type: "+type +" Payment details are "+paymentList);
            return new ResponseEntity<List<Payment>>(paymentList, HttpStatus.OK);
        }
    }

    @Override
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public int save(Payment payment) {
        logger.info("Save Payment is called");
        return paymentService.save(payment);
    }
}
